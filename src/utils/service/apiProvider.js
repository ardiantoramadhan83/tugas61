import axios from 'axios';
import {BASE_URL, TOKEN} from '../service/Url';

const API = async (
  url,
  optios = {
    method: 'GET',
    body: {},
    head: {},
  },
) => {
  const request = {
    baseURL: BASE_URL,
    method: optios.method,
    timeout: 10000,
    url,
    headers: optios.head,
    responseType: 'json',
  };
  if (
    request.method === 'POST' ||
    request.method === 'PUT' ||
    request.method === 'DELETE'
  )
    request.data = optios.body;

  const res = await axios(request);

  if (res.status === 200) {
    return res.data;
  } else {
    return res;
  }
};

export default {
  getDataMobil: async () => {
    return API('mobil', {
      method: 'GET',
      head: {
        'Contenty-Type': 'application/json',
        Authorization: TOKEN,
      },
    })
      .then(response => {
        return response;
      })
      .catch(err => {
        return err;
      });
  },

  postData: async body => {
    return API('mobil', {
      method: 'POST',
      body: body,
      head: {
        'Contenty-Type': 'application/json',
        Authorization: TOKEN,
      },
      body: body,
    })
      .then(response => {
        return response;
      })
      .catch(err => {
        return err;
      });
  },

  editData: async body => {
    return API('mobil', {
      method: 'PUT',
      body: body,
      head: {
        'Contenty-Type': 'application/json',
        Authorization: TOKEN,
      },
      body: body,
    })
      .then(response => {
        return response;
      })
      .catch(err => {
        return err;
      });
  },

  deleteData: async body => {
    return API('mobil', {
      method: 'DELETE',
      body: body,
      head: {
        'Contenty-Type': 'application/json',
        Authorization: TOKEN,
      },
      body: body,
    })
      .then(response => {
        return response;
      })
      .catch(err => {
        return err;
      });
  },
};
