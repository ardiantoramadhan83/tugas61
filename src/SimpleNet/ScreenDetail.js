import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  ImageBackground,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Modal,
  ActivityIndicator,
  Image,
} from 'react-native';
import {BASE_URL, TOKEN} from '../SimpleNet/Url';
import Icon from 'react-native-vector-icons/AntDesign';
import Axios from 'axios';
import apiProvider from '../utils/service/apiProvider';
import {convertCurrency} from '../utils/service/helper';
import Carousel from 'react-native-snap-carousel';

const ScreenDetail = ({navigation, route}) => {
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  var dataMobil = route.params;
  console.log('ini data moil', dataMobil);

  const editData = async () => {
    setIsLoading(true);
    if (!namaMobil || !hargaMobil || !totalKM) {
      alert('Semua kolom harus diisi!');
      setIsLoading(false);
      return;
    }

    if (hargaMobil < 20000000) {
      alert('Harga mobil tidak boleh kurang dari 20 juta!');
      setIsLoading(false);
      return;
    }

    const body = [
      {
        _uuid: dataMobil._uuid,
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];

    const response = await apiProvider.editData(body);
    console.log('[atr LOG]: data', response);
    if (response?.items?.length) {
      console.log('response edit success', response);
      if (response.status === 200 || response.status === 201) {
        setNamaMobil('');
        setTotalKM('');
        setHargaMobil('');
      }
    }
    alert('Data Mobil berhasil dirubah');
    setIsModalVisible('false');
    navigation.navigate('Home');
  };

  const deleteData = async () => {
    setIsLoading(true);
    const body = [
      {
        _uuid: dataMobil._uuid,
      },
    ];

    const response = await apiProvider.deleteData(body);
    console.log('[atr LOG]: data', response);
    if (response?.items?.length) {
      console.log('response delete success', response);
      if (response.status === 200 || response.status === 201) {
      }
    }
    alert('Data Mobil berhasil dihapus');
    navigation.navigate('Home');
  };

  useEffect(() => {
    if (route.params) {
      const data = route.params;
      console.log('datamobil', data);
      setNamaMobil(data.title);
      setTotalKM(data.totalKM);
      setHargaMobil(data.harga);
    }
  }, []);

  // carousel start
  const CarouselCardItem = ({item, index}) => {
    return (
      <TouchableOpacity onPress={() => navigation.navigate('Detail', item)}>
        <View style={styles.carouselcontainer} key={index}>
          <Image source={{uri: item.unitImage}} style={styles.carouselimage} />
          <Text style={styles.carouselheader}>{item.title}</Text>
          <Text style={styles.carouselbody}>
            {convertCurrency(item.harga, 'Rp. ')}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  const isCarousel = React.useRef(null);
  const [dataMobilM, setDataMobilM] = useState([]);
  useEffect(() => {
    getDataMobil();
  }, []);
  const getDataMobil = async () => {
    const response = await apiProvider.getDataMobil();
    console.log('LOG CAROUSEL:', response);
    if (response?.items?.length) {
      setDataMobilM(response.items);
    } else {
      alert('Tidak dapat memuat data. Cek koneksi internet Anda.');
    }
  };
  // carousel end

  return (
    <View style={{flex: 1, backgroundColor: '#ffffff'}}>
      <ScrollView
        showsVerticalScrollIndicator={true}
        contentContainerStyle={{paddingBottom: 10}}>
        {dataMobil?.unitImage?.length > 0 && (
          <ImageBackground
            source={{uri: dataMobil.unitImage}}
            style={{
              width: Dimensions.get('window').width,
              height: 316,
              width: '100%',
            }}>
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-start',
                padding: 10,
              }}>
              <TouchableOpacity
                onPress={() => navigation.goBack()}
                style={{
                  width: '10%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingVertical: 10,
                }}>
                <Icon name="arrowleft" size={20} color="#000" />
              </TouchableOpacity>
              <Text style={{fontSize: 16, fontWeight: 'bold', color: '#000'}}>
                Detail
              </Text>
            </View>
          </ImageBackground>
        )}
        <View
          style={{
            width: '100%',
            backgroundColor: '#ffffff',
            borderTopLeftRadius: 25,
            borderTopRightRadius: 25,
            paddingHorizontal: 20,
            paddingTop: 10,
            marginTop: -25,
            height: '100%',
            flex: 1,
          }}>
          <Text
            style={{
              fontSize: 17,
              fontWeight: '500',
              color: '#201F26',
            }}>
            {dataMobil.title}
          </Text>
          <Text
            style={{
              color: '#595959',
              marginTop: 10,
              fontSize: 15,
            }}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Massa
            gravida mattis arcu interdum lectus egestas scelerisque. Blandit
            porttitor diam viverra amet nulla sodales aliquet est. Donec enim
            turpis rhoncus quis integer. Ullamcorper morbi donec tristique
            condimentum ornare imperdiet facilisi pretium molestie.
          </Text>
          <View
            style={{
              flexDirection: 'row',
            }}>
            <Text
              style={{
                color: '#201F26',
                fontWeight: '500',
                fontSize: 17,
                marginTop: 23,
              }}>
              Total KM :
            </Text>
            <Text
              style={{
                color: '#201F26',
                fontWeight: '500',
                fontSize: 17,
                marginTop: 23,
                marginLeft: 10,
              }}>
              {dataMobil.totalKM}
            </Text>
          </View>
          <Text
            style={{
              color: '#8D8D8D',
              fontSize: 17,
            }}>
            {convertCurrency(dataMobil.harga, 'Rp. ')}
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => setIsModalVisible(true)}
          style={{
            width: 335,
            justifyContent: 'center',
            alignContent: 'center',
            marginTop: 10,
            backgroundColor: '#BB2427',
            borderRadius: 8,
            paddingVertical: 15,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 35,
            alignSelf: 'center',
          }}>
          <Text
            style={{
              color: '#fff',
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            EDIT DATA
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => deleteData() == navigation.navigate('Home')}
          style={{
            width: 335,
            justifyContent: 'center',
            alignContent: 'center',
            marginTop: 10,
            backgroundColor: '#000000',
            borderRadius: 8,
            paddingVertical: 15,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 35,
            marginBottom: 35,
            alignSelf: 'center',
          }}>
          <Text
            style={{
              color: '#fff',
              fontSize: 16,
              fontWeight: 'bold',
            }}>
            DELETE DATA
          </Text>
        </TouchableOpacity>
        <Modal visible={isModalVisible} transparent={true}>
          <View
            style={{
              padding: 25,
              marginHorizontal: 25,
              marginTop: 100,
              justifyContent: 'flex-start',
              textAlign: 'left',
              backgroundColor: 'white',
              borderRadius: 10,
              borderColor: 'red',
              borderWidth: 1,
            }}>
            <View
              style={{
                width: '100%',
                padding: 15,
                backgroundColor: '#FFFFFF',
                alignSelf: 'center',
                justifyContent: 'center',
                alignContent: 'center',
              }}>
              <View>
                <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                  Nama Mobil
                </Text>
                <TextInput
                  onChangeText={text => setNamaMobil(text)}
                  value={namaMobil}
                  placeholder="Masukkan Nama Mobil"
                  style={styles.txtInput}
                />
              </View>
              <View style={{marginTop: 20}}>
                <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                  Total Kilometer
                </Text>
                <TextInput
                  onChangeText={text => setTotalKM(text)}
                  value={totalKM}
                  placeholder="contoh: 100 KM"
                  style={styles.txtInput}
                  keyboardType="number-pad"
                />
              </View>
              <View style={{marginTop: 20}}>
                <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                  Harga Mobil
                </Text>
                <TextInput
                  onChangeText={text => setHargaMobil(text)}
                  value={hargaMobil}
                  placeholder="Masukkan Harga Mobil"
                  style={styles.txtInput}
                  keyboardType="number-pad"
                />
              </View>
              <TouchableOpacity
                onPress={() => editData()}
                style={styles.btnAdd}>
                <Text style={{color: '#fff', fontWeight: '600'}}>
                  Edit Data
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setIsModalVisible(false)}
                style={[styles.btnAdd, {backgroundColor: 'grey'}]}>
                <Text style={{color: '#fff', fontWeight: '600'}}>Cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <Text style={{marginTop: 20, marginLeft: 20}}>Recent Listed Cars:</Text>
        <View style={{alignItems: 'center'}}>
          <Carousel
            layout="default"
            layoutCardOffset={5}
            ref={isCarousel}
            data={dataMobilM}
            renderItem={CarouselCardItem}
            sliderWidth={300}
            itemWidth={300}
            inactiveSlideShift={0}
            useScrollView={true}
          />
        </View>
      </ScrollView>

      {isLoading ? (
        <Modal visible={true} transparent={true}>
          <View
            style={{
              width: '100%',
              height: '100%',
              backgroundColor: 'rgba(0,0,0,0.3)',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                backgroundColor: 'white',
                width: 50,
                height: 50,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 5,
              }}>
              <ActivityIndicator size="large" color="green" />
            </View>
          </View>
        </Modal>
      ) : (
        <View></View>
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
  carouselcontainer: {
    backgroundColor: 'white',
    borderRadius: 8,
    width: 300,
    paddingBottom: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 7,
    marginVertical: 10,
    alignItems: 'center',
  },
  carouselimage: {
    width: Dimensions.get('screen').width,
    height: 150,
  },
  carouselheader: {
    color: '#222',
    fontSize: 24,
    fontWeight: 'bold',
    paddingLeft: 15,
    paddingTop: 15,
  },
  carouselbody: {
    color: '#222',
    fontSize: 14,
    paddingLeft: 15,
    paddingRight: 15,
  },
});

export default ScreenDetail;
