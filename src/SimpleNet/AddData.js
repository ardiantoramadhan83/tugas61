import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {BASE_URL, TOKEN} from './Url';
import Axios from 'axios';
import apiProvider from '../utils/service/apiProvider';

const AddData = ({navigation, route}) => {
  var dataMobil = route.params;
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');

  useEffect(() => {
    if (route.params) {
      const data = route.params;
      setNamaMobil(data.title);
      setTotalKM(data.totalKM);
      setHargaMobil(data.harga);
    }
  }, []);

  const postData = async () => {
    if (namaMobil == '') {
      alert('Nama Mobil tidak boleh kosong');
    } else if (totalKM == '') {
      alert('Kilometer tidak boleh kosong');
    } else if (hargaMobil == '') {
      alert('Harga Mobil tidak boleh kosong');
    } else if (parseInt(hargaMobil) < 100000000) {
      alert('Harga mobil tidak boleh kurang dari 100 juta');
    } else {
      const body = [
        {
          title: namaMobil,
          harga: hargaMobil,
          totalKM: totalKM,
          unitImage:
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
        },
      ];

      const response = await apiProvider.postData(body);
      console.log('[atr LOG]: data', response);
      if (response?.items?.length) {
        setNamaMobil('');
        setTotalKM('');
        setHargaMobil('');
      }
      alert('Data Mobil Berhasil ditambah');
      navigation.navigate('Home');
    }
  };

  // const editData = async () => {
  //   const body = [
  //     {
  //       _uuid: dataMobil._uuid,
  //       title: namaMobil,
  //       harga: hargaMobil,
  //       totalKM: totalKM,
  //       unitImage:
  //         'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
  //     },
  //   ];
  //   const options = {
  //     headers: {
  //       'Content-Type': 'application/json',
  //       Authorization: TOKEN,
  //     },
  //   };
  //   Axios.post(`${BASE_URL}mobil`, body, options)
  //     .then(response => {
  //       console.log('response add success', response);
  //       if (response.status === 200 || response.status === 201) {
  //         alert('Data Mobil berhasil diubah');
  //         navigation.goBack();
  //       }
  //     })
  //     .catch(error => {
  //       console.log(`error add`, error);
  //     });
  // };

  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{
            width: '10%',
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10,
          }}>
          <Icon name="arrowleft" size={20} color="#000" />
        </TouchableOpacity>
        <Text style={{fontSize: 16, fontWeight: 'bold', color: '#000'}}>
          {dataMobil ? 'Ubah Data' : 'Tambah Data'}
        </Text>
      </View>
      <View
        style={{
          width: '100%',
          padding: 15,
        }}>
        <View>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Nama Mobil
          </Text>
          <TextInput
            placeholder="Masukkan Nama Mobil"
            onChangeText={text => setNamaMobil(text)}
            style={styles.txtInput}
            value={namaMobil}
          />
        </View>
        <View style={{marginTop: 20}}>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Total Kilometer
          </Text>
          <TextInput
            placeholder="contoh: 100 KM"
            onChangeText={text => setTotalKM(text)}
            style={styles.txtInput}
            value={totalKM}
          />
        </View>
        <View style={{marginTop: 20}}>
          <Text style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
            Harga Mobil
          </Text>
          <TextInput
            onChangeText={text => setHargaMobil(text)}
            placeholder="Masukkan Harga Mobil"
            style={styles.txtInput}
            keyboardType="number-pad"
            value={hargaMobil}
          />
        </View>
        <TouchableOpacity
          style={styles.btnAdd}
          onPress={() => (dataMobil ? editData() : postData())}>
          <Text style={{color: '#fff', fontWeight: '600'}}>
            {dataMobil ? 'ubahData' : 'Tambah Data'}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtInput: {
    marginTop: 10,
    width: '100%',
    borderRadius: 6,
    paddingHorizontal: 10,
    borderColor: '#dedede',
    borderWidth: 1,
  },
});

export default AddData;
