import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import Home from './Home';
import AddData from './AddData';

import ScreenDetail from './ScreenDetail';

const Stack = createNativeStackNavigator();

export default function Routing() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="AddData" component={AddData} />

        <Stack.Screen name="ScreenDetail" component={ScreenDetail} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
